
`kernelci-regressions-tracker` is an experimental tool to improve the state of regression tracking in KernelCI and the kernel community at large.

Deployed at [https://kernel.pages.collabora.com/kernelci-regressions-tracker/](https://kernel.pages.collabora.com/kernelci-regressions-tracker/).

# About

Our `kernelci-regressions-tracker` relies on existing KernelCI data from *kernelci-results* email reports and KernelCI database to build a more comprehensive data visualization and regression tracking experimental dashboard compared to what is offered by the current KernelCI [web dashboard](https://linux.kernelci.org/).

DISCLAIMER: This work is **experimental**. Users should not rely fully on its data or link to it from any immutable storage. We do not intend to evolve it as permanent parallel infrastructure. Instead, while the new API, Pipeline and Dashboard are not ready, it is a proposition to facilitating regressions tracking by removing manual work and storing metadata about the regressions over time.

The tracker also intends to automatically identify infrastructure failures and test flakiness, helping reporters and lab owners to be more efficient.

Before the `kernelci-regressions-tracker` come to existence, the regression tracking work was mostly manual with repeating tasks (such as navigate through KernelCI Web Dashboard reports going over regressions that were already identified before but no good tracking was available to keep these information over time).

For this project, we are approaching this work as a continuous process. As we practice, we learn what the next steps are.

Finally, here's a few goals and directions of our overall Regression Tracking initiative:

* Improve the regression tracker itself, adding more small-wins in automation and data intelligence;
* Refining our reporting process to the community, including improving our interaction with [regzbot](https://linux-regtracking.leemhuis.info/regzbot/);
* Gather feedback from the community and improve [regression reporting template](docs/regression-reporting-template.txt);
* Understand the current limitations of the regressions tracking process of the community at large and engage in discussions and initiatives to help improve it as a whole;
* Generate statistics about our Regression Reporting work to show to the community and other interested parties;
* Develop knowledge about Regression Tracking user flows and contribute to the discussions of the new KernelCI Web Dashboard through the [UX dashboard](https://github.com/orgs/kernelci/projects/18);
* Develop relationship with interested parties across the Ecosystem be it kernel developers, users or companies struggling with kernel regressions.

# Installing, running and developing

See [install-and-develop.md](docs/install-and-develop.md) for detailed instructions.

For deployment see the [gitlab-ci](.gitlab-ci.yml) file.

# Maintainers

* Gustavo Padovan < gustavo.padovan@collabora.com >
* Ricardo Cañuelo < ricardo.canuelo@collabora.com >
