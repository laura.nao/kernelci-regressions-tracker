# Installing

## Dependencies

On a python 3.10 (or higher) environment run:

    pip install -r requirements.txt

## groups.io authentication

We pull data from the groups.io email archives. You'll need a cookie token for it.
Get your cookie file as stated in their [docs](https://groups.io/api#login):

    curl "https://groups.io/api/v1/login" -c "cookies" -d "email=user@example.com&password=12345678"

Leave the `cookies` file in the root folder of this project.

## KernelCI database token

To extract data from the (old) KernelCI database through its API you
need a token to the API endpoint you want to query. You can request one
from the KernelCI SysAdmin folks. Then, to use it define the appropriate
environment variable with the token value. For example, to define the
token for api.kernelci.org, do:

    export TOKEN_api_kernelci_org="<token>"

to define it for api.chromeos.kernelci.org, do:

    export TOKEN_api_chromeos_kernelci_org="<token>"

# Running

`regtracker` will generate html reports based no the tracked trees listed at `configs/tracked-branches.yaml`. By default it will fetch 100 messages from the kernelci-results mail archive and output to a timestamped dir (eg: `output-20230224-1445+0000`).

To change these configurations run:

    ./regtracker run --branches configs/tracked-branches-test.yaml --output output --messages 2000

For detailed instructions run `./regtracker run -h`


# Developing and Testing

To speed up development and testing, dump and load command line arguments were created to save emails fetched from groups.io and store the full processed data used to generate the html reports.

To create a messages dump for 2000 messages run:

    ./regtracker run --messages-dump messages-dump.pickle -m 2000

Then, to avoid downloading the messages in subsequent executions:

    ./regtracker run --messages-load messages-dump.pickle

The `--messages-load` command ignores `--messages`.


# Matching failure strings

As part of the regression tracker, we want to automate as much as possible the identification of false-positive regressions. Unfortunately, KernelCI still has many infrastructure errors reported as regressions.

The `logs-string-match.yaml` file catalogs different categories of failure and their strings. `patterns` is a list of string lists. We use string list because we main want to see more than one line occur in the same log to be able to classify that failure properly. The code makes sure it can find all strings in the string list.

To run the classifier independently:

    ./regtracker classify-log https://storage.kernelci.org/mainline/master/v6.2-rc8-15-gf6feea56f66d/x86_64/x86_64_defconfig+rust-samples/rustc-1.62/lab-baylibre/baseline-qemu_x86_64-uefi.txt

or with a local log file:

    ./regtracker classify-log local_log.txt

# Running custom KernelCI queries

The `kci-query` subcommand can be used to query the KernelCI API
directly to get info about regressions and test cases.

To run it you'll need to have a KernelCI API authentication token (see
section "KernelCI database token" above).

It currently supports info retrieval for regressions and test runs
providing their id, for instance:

    ./regtracker kci-query -t <token> --regression-id 645c6250e1bf6e5ce62e8622
    ./regtracker kci-query -t <token> --test-id 64724fbbb65e70423f2e864c

and also searching for test runs providing the necessary search
parameters:

    ./regtracker kci-query -t <token> --tree mainline --branch master --arch arm64 --test-name baseline-nfs.dmesg.alert --kernel v6.4-rc3-291-g4e893b5aa4ac2

Run `./regtracker kci-query --help` for more info.

