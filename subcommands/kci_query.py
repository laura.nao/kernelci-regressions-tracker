# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import logging
import sys
import json
import requests

import common.legacy_kci_api as api
from common.models.legacy_kci_testcase import LegacyKCITestCase, find_test_cases
from common.models.legacy_kci_regression import LegacyKCIRegression

def run_query(args):
    if not args.api_url:
        sys.exit("No KernelCI API URL specified: "
                 "specify it using the -a cmdline parameter")

    #api.set_backend_url(args.api_url)

    # Regression id provided: get regression info
    if args.regression_id:
        regression = LegacyKCIRegression.from_id(args.api_url, args.regression_id)
        print(regression)
        if not args.summary:
            for t in regression.tests:
                print(t)
                print()
        return
    # Test id provided: get test info
    if args.test_id:
        testcase = LegacyKCITestCase.from_id(args.api_url, args.test_id)
        print(testcase)
        return
    # Otherwise: find test based on the provided query parameters
    try:
        tests = find_test_cases(arch=args.arch,
                                tree=args.tree,
                                branch=args.branch,
                                device_type=args.device_type,
                                lab_name=args.lab_name,
                                test_name=args.test_name,
                                defconfig=args.defconfig,
                                kernel=args.kernel,
                                status=args.status,
                                date_range=args.date_range,
                                retry=False)
        for t in tests:
            print(t)
    except requests.exceptions.HTTPError as err:
        if err.response.status_code == 504:
            logging.error("Query timed out. Try refining the search parameters")
            sys.exit(1)
        raise

def rt_add_kci_query_cmd(subparsers):
    parser = subparsers.add_parser('kci-query',
                help="run a KernelCI query and print the results")
    parser.add_argument('-a', '--api-url', help=("KernelCI API URL to connect to"))

    test_id_grp = parser.add_argument_group("Testcase info", "Testcase data retrieval")
    regressionid_grp = parser.add_argument_group("Regression info", "Regression data retrieval")
    custom_query_grp = parser.add_argument_group("Custom query", "Custom KernelCI query options")
    test_id_grp.add_argument('--test-id', type=str, metavar="TEST_ID",
                             help="Id of the KernelCI test case to query")
    regressionid_grp.add_argument('--regression-id', type=str, metavar="REGRESSION_ID",
                                  help="Id of the KernelCI regression to query")
    regressionid_grp.add_argument('--summary', action='store_true',
                                  help="Show only the regression summary")
    custom_query_grp.add_argument('--arch', type=str)
    custom_query_grp.add_argument('--tree', type=str)
    custom_query_grp.add_argument('--branch', type=str)
    custom_query_grp.add_argument('--device-type', type=str)
    custom_query_grp.add_argument('--lab-name', type=str)
    custom_query_grp.add_argument('--test-name', type=str)
    custom_query_grp.add_argument('--defconfig', type=str)
    custom_query_grp.add_argument('--kernel', type=str)
    custom_query_grp.add_argument('--status', type=str)
    custom_query_grp.add_argument('--date-range', type=int)
    parser.set_defaults(func=run_query)
