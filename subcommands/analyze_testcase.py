#!/usr/bin/env python3
#
# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from datetime import datetime, timezone
import os
import shutil
import sys
import urllib

import jinja2

import common.legacy_kci_api as api
from common.models.legacy_kci_testcase import LegacyKCITestCase
from common.analysis import analyze_testcase


def _generate_report(test, info, days, outdir=None):
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.join(sys.path[0], 'templates')))
    if not outdir:
        tstamp = datetime.now(timezone.utc).strftime('%Y%m%d-%H%M%S')
        dirname = f"{test.test_id}_{tstamp}"
    else:
        dirname = outdir
    try:
        os.mkdir(dirname)
    except FileExistsError:
        shutil.rmtree(dirname)
        os.mkdir(dirname)
    shutil.copy(os.path.join('templates', 'main.css'), dirname)

    with open(os.path.join(dirname, 'analysis.html'), 'w') as out:
        template = env.get_template('analysis.html.j2')
        html_output = template.render(now=datetime.now(timezone.utc),
                                      reference=test,
                                      history=info['history'],
                                      days=days,
                                      diff_defconfigs=info['diff_defconfigs'],
                                      diff_dev_types=info['diff_dev_types'],
                                      tests_same_device=info['tests_same_device'])
        out.write(html_output)

def run_analyze_testcase(args):
    if args.token:
        api.set_auth_token(args.token)
    if not api.auth_token_ok():
        sys.exit("KernelCI authentication token needed: "
                 "define the KERNELCIAPI_TOKEN environment variable "
                 "or use the -t cmdline parameter")
    # Get testcase id from the link and find testcase info
    urlpath = urllib.parse.urlparse(args.link.strip('/')).path
    testcase_id = urlpath.rpartition('/')[2]
    test = LegacyKCITestCase.from_id(testcase_id)
    info = analyze_testcase(test, args.days)
    _generate_report(test, info, days=args.days, outdir=args.output)

def rt_add_analyze_testcase_cmd(subparsers):
    parser = subparsers.add_parser('analyze-testcase',
                                   help=("runs an analysis of a KernelCI test "
                                         "case and generates an html output"))
    parser.add_argument('-t', '--token',
                        help="Auth token for api.kernelci.org")
    parser.add_argument('-o', '--output',
                        help="output dir for the HTML report")
    parser.add_argument('--days', type=int, default=1,
                        help="Number to days before the test to look for")
    parser.add_argument('link',
                        help=("KernelCI link to the test case to analyze (for example: "
                              "https://kernelci.org/test/case/id/640bbcd2f5276084f18c86ee/)"))
    parser.set_defaults(func=run_analyze_testcase)
