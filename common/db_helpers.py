# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from datetime import datetime, timezone
import sqlite3

from common.models.bisection import Bisection
from common.models.legacy_kci_regression import LegacyKCIRegression, LegacyKCIRegressionMetadata
from common.models.legacy_kci_testcase import LegacyKCITestCase
from common.utils import process_date_param

conn = None
db_info_table = 'db_info'

def db_setup(db='regtracker.db'):
    global conn

    conn = sqlite3.connect(db)
    # Uncomment to enable query debug traces:
    # conn.set_trace_callback(print)
    Bisection.create_table(conn)
    LegacyKCIRegression.create_table(conn, Bisection.db_table,
                                     "message_id")
    LegacyKCIRegressionMetadata.create_table(conn,
                                             LegacyKCIRegression.db_table,
                                             "api_url, regression_id")
    LegacyKCITestCase.create_table(conn, LegacyKCIRegression.db_table,
                                   "api_url, regression_id")
    # Create db_info table
    cur = conn.cursor()
    cur.execute(f"""CREATE TABLE IF NOT EXISTS {db_info_table} (
    attribute TEXT PRIMARY KEY,
    value     TEXT);""")
    return conn

def db_conn():
    return conn

def db_set_last_update_timestamp(tstamp=None):
    cur = conn.cursor()
    if not tstamp:
        tstamp = datetime.now(timezone.utc)
    tstamp_str = tstamp.strftime("%Y-%m-%d %H:%M:%S.%f")
    cur = conn.cursor()
    cur.execute(f"REPLACE INTO {db_info_table} VALUES(?,?)",
                ('last_update', tstamp_str))
    conn.commit()

def db_get_last_update_timestamp():
    cur = conn.cursor()
    result = cur.execute(f"SELECT value FROM {db_info_table} "
                         f"WHERE attribute='last_update'")
    if result:
        return process_date_param(result.fetchone()[0])
    return None

def db_get_oldest_regression_timestamp():
    results = LegacyKCIRegression.custom_query(db_conn(),
            fields=["date"],
            extra="order by date limit 1")
    if results:
        return process_date_param(results[0][0])
    return None
