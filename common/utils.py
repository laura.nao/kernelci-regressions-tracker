# Copyright (C) 2023 Collabora Limited
# Author: Gustavo Padovan <gustavo.padovan@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from datetime import datetime, timezone
import logging
import os
import re
import requests
import yaml

from requests.adapters import HTTPAdapter
from yaml.loader import SafeLoader
from urllib.parse import urlparse
from urllib3 import Retry

def _get_failure_type(string_match, found):
    found = set(found)
    for category in string_match['categories']:
        for pattern_list in category['patterns']:
            matches = 0
            for line in found:
                for pattern in pattern_list:
                    if re.search(pattern, line):
                        matches += 1

            if matches == len(pattern_list):
                return category['tag']

    return "unknown"

def classify_failure(log):
    with open(os.path.join("configs", "logs-string-match.yaml")) as f:
        string_match = yaml.load(f, Loader=SafeLoader)

    l = []
    for category in string_match['categories']:
        for pattern in category['patterns']:
            l.append('|'.join(pattern))

    m = re.findall("|".join(l), log)
    return _get_failure_type(string_match, m), m

def check_for_failures(plain_log):
    """Check regression log for failures.

    This fetches the logs from KernelCI and calls classify_failure()
    to parse and check.

    Args:
      plain_log (str): the url of the log to download.

    Returns:
      A tuple with the <regression type>, [list of strings matches].
    """
    response = requests.get(plain_log)
    if response.status_code != 200:
        logging.error(f"Unable to download regression log: {plain_log}")
        return "unknown", []
    content = response.content.decode("utf-8")
    return classify_failure(content)

def start_https_session(server, cookie_jar=None, retry=True):
    if retry:
        adapter = HTTPAdapter(max_retries=Retry(
            total=5,
            backoff_factor=30,
            status_forcelist=[429, 500, 502, 503, 504]))
    else:
        adapter = HTTPAdapter()

    session = requests.Session()
    session.mount(server, adapter)

    if cookie_jar:
        session.cookies = cookie_jar

    return session

def _is_url_invalid(url):
    try:
        result = urlparse(url)
        return not all([result.scheme, result.netloc])
    except:
        return True

def get_http_data(session, url, params=None, headers=None):
    if _is_url_invalid(url):
        logging.error(f"Invalid URL: <{url}>.")
        raise requests.exceptions.InvalidURL

    response = session.get(url, params=params, headers=headers)
    response.raise_for_status()
    return response

def process_date_param(date):
    """Returns a datetime object containing the date passed as a parameter,
    which can be given as:
      - a datetime object (returned as is)
      - an ISO 8601 format string
      - the number of milliseconds since the epoch as an integer
    Returns None if the date parameter is None
    """
    if date:
        if type(date) == datetime:
            return report_date
        else:
            try:
                # If the date was given as an ISO 8601 format string
                return datetime.fromisoformat(date)
            except TypeError:
                # Assume it's the number of milliseconds since the epoch
                # (KernelCI format)
                return datetime.fromtimestamp(date / 1000, timezone.utc)
    return None
