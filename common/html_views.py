#!/usr/bin/env python3
#
# Copyright (C) 2023 Collabora Limited
# Author: Gustavo Padovan <gustavo.padovan@collabora.com>
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from datetime import datetime, timedelta, timezone
import logging
import os
import shutil
import sys

import jinja2

import common.db_helpers as db_helpers
from common.db_helpers import db_conn
from common.models.bisection import Bisection
from common.models.legacy_kci_regression import LegacyKCIRegression

def _process_infra_issues():
    # Collect all uncategorized regressions and infra issues
    regressions = LegacyKCIRegression.search(db_conn(),
                                             match_params={'category' : 'unknown'})
    infra_issues = LegacyKCIRegression.search(db_conn(),
                                              query_cond=(f"category LIKE 'infra%'"))

    # Collect all regressions and infra issues with 'lab' as key
    labs = []
    res = LegacyKCIRegression.custom_query(db_conn(),
                                           fields=['distinct lab'],
                                           extra='ORDER BY lab')
    for r in res:
        labs.append(r[0])
    infra_issues_per_lab = {}
    regressions_per_lab = {}
    for lab in labs:
        infra_issues_per_lab[lab] = []
        regressions_per_lab[lab] = []
        regs = LegacyKCIRegression.search(db_conn(),
                                          query_cond=(f"lab = '{lab}' AND "
                                                      f"category LIKE 'infra%'"))
        for r in regs:
            infra_issues_per_lab[lab].append(r)
        regs = LegacyKCIRegression.search(db_conn(),
                                          match_params={'lab' : lab,
                                                        'category' : 'unknown'})
        for r in regs:
            regressions_per_lab[lab].append(r)

    # Collect all regressions and infra issues with 'device_type' as key
    platforms = []
    res = LegacyKCIRegression.custom_query(db_conn(),
                                           fields=['distinct device_type'],
                                           extra='ORDER BY device_type')
    for r in res:
        platforms.append(r[0])
    infra_issues_per_platform = {}
    regressions_per_platform = {}
    for p in platforms:
        infra_issues_per_platform[p] = []
        regressions_per_platform[p] = []
        regs = LegacyKCIRegression.search(db_conn(),
                                          query_cond=(f"device_type = '{p}' AND "
                                                      f"category LIKE 'infra%'"))
        for r in regs:
            infra_issues_per_platform[p].append(r)
        regs = LegacyKCIRegression.search(db_conn(),
                                          match_params={'device_type' : p,
                                                        'category' : 'unknown'})
        for r in regs:
            regressions_per_platform[p].append(r)

    # Collect all infra issues with 'category' as key
    category_types = []
    res = LegacyKCIRegression.custom_query(db_conn(),
                                           fields=['distinct category'],
                                           cond=f"category LIKE 'infra%'",
                                           extra='ORDER BY category')
    for r in res:
        category_types.append(r[0])
    infra_issues_per_category = {}
    for c in category_types:
        infra_issues_per_category[c] = []
        regs = LegacyKCIRegression.search(db_conn(),
                                          match_params={'category' : c})
        for r in regs:
            infra_issues_per_category[c].append(r)

    # build dict with spliting infra issues type per lab
    issues_per_category_and_lab = {}
    for c in category_types:
        issues_per_category_and_lab[c] = {key: 0 for key in labs}
        for l in labs:
            res = LegacyKCIRegression.search(db_conn(),
                                             match_params={'category' : c,
                                                           'lab' : l})
            issues_per_category_and_lab[c][l] = len(res)

    return {
        'infra_issues'                 : infra_issues,
        'uncategorized_regressions'    : regressions,
        'labs'                         : labs,
        'infra_issues_per_lab'         : infra_issues_per_lab,
        'regressions_per_lab'          : regressions_per_lab,
        'platforms'                    : platforms,
        'infra_issues_per_platform'    : infra_issues_per_platform,
        'regressions_per_platform'     : regressions_per_platform,
        'category_types'               : category_types,
        'infra_issues_per_category'    : infra_issues_per_category,
        'issues_per_category_and_lab'  : issues_per_category_and_lab
    }

def _regression_data_per_branch():
    """Processes the regressions in the DB and extracts summary data grouped
    by repo branch. The summary data extracted contains:
      - the number of new regressions
      - the number of regressions known to be bisected
      - the number of regressions classified as infra errors
      - the regressions for this branch grouped by test name and first
        test failed

    The results are returned as a list of dicts, where each list element
    is a dict with the summary data of a repo branch.
    """
    one_day_range = datetime.now(timezone.utc) - timedelta(days=1)
    results = LegacyKCIRegression.custom_query(db_conn(),
            fields=["tree", "branch"],
            extra="GROUP BY tree, branch")
    if results:
        regression_data = []
        for r in results:
            regression_data.append({'tree' : r[0], 'branch' : r[1]})
    else:
        return None
    for d in regression_data:
        # Get all regressions
        d['regressions'] = LegacyKCIRegression.search(db_conn(),
                                match_params={'tree'   : d['tree'],
                                              'branch' : d['branch']})
        d['new'] = 0
        d['bisected'] = 0
        d['infra'] = 0
        table = str.maketrans({'/' : '-',
                               ':' : '-'})
        d['page'] = f"{d['tree'].translate(table)}-{d['branch'].translate(table)}.html"
        for r in d['regressions']:
            # New regressions per branch
            if r.days_since_first_failed() < 2:
                d['new'] += 1
            # Bisected regressions per branch
            if r.bisection_id:
                d['bisected'] += 1
            # Infra failures per branch
            if r.category and r.category != 'unknown':
                d['infra'] += 1

        # Regressions in this branch grouped by test name and first test
        # failed
        d['by_test'] = {}
        for r in d['regressions']:
            if r.test_name not in d['by_test']:
                d['by_test'][r.test_name] = {}
            if r.first_failed().kernel not in d['by_test'][r.test_name]:
                d['by_test'][r.test_name][r.first_failed().kernel] = [r]
            else:
                d['by_test'][r.test_name][r.first_failed().kernel].append(r)
    return regression_data

def generate_html_reports(output_dir, bisections, analysis_cache, status):
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.join(sys.path[0], 'templates')))
    now = datetime.now(timezone.utc)

    if output_dir:
        dir = output_dir
        if os.path.exists(dir):
            shutil.rmtree(dir)
    else:
        dir = "output-{}".format(now.strftime("%Y%m%d-%H%M%z"))
    os.mkdir(dir)

    # Oldest regression date in DB
    oldest_date = db_helpers.db_get_oldest_regression_timestamp()
    # Last DB update
    last_update = db_helpers.db_get_last_update_timestamp()

    # Collect regression info for each branch
    regression_data = _regression_data_per_branch()
    if not regression_data:
        logging.info("No regressions found: no output generated")
        return

    # Index page
    with open(os.path.join(dir, 'index.html'), 'w') as output_file:
        template = env.get_template('index.html.j2')
        html_output = template.render(now=now,
                                      oldest_date=oldest_date,
                                      last_update=last_update,
                                      regressions=regression_data)
        output_file.write(html_output)
    # Regression analysis: one page per regression "group", where a
    # group is any number of regressions from the same tree/branch
    # having the same test name and first test failed
    for regressions_in_branch in regression_data:
        for test_name in regressions_in_branch['by_test']:
            for kernel, reg_list in regressions_in_branch['by_test'][test_name].items():
                # If any of the regressions in the group is linked to a
                # bisection report, use that report for all the
                # regressions in the group
                bisection_id = ""
                for r in reg_list:
                    if r.bisection_id:
                        bisection_id = r.bisection_id
                        break
                if bisection_id:
                    for r in reg_list:
                        r.bisection_id = bisection_id
                regression = reg_list[0]
                regression_page = os.path.join(
                    dir, f"{regression.generic_id_str(url_safe=True)}.html")
                with open(regression_page, 'w') as output_file:
                    if regression.generic_id_str() in analysis_cache:
                        analysis = analysis_cache[regression.generic_id_str()]
                    else:
                        analysis = {}
                    analysis['days'] = 1
                    results = Bisection.search(db_conn(),
                                               match_params={'message_id' : regression.bisection_id})
                    if results:
                        analysis['bisection'] = results[0]
                    template = env.get_template('regression.html.j2')
                    html_output = template.render(now=now, regression=regression, **analysis)
                    output_file.write(html_output)

    # Regression indexes: one page per branch
    for regressions_in_branch in regression_data:
        regressions_page = os.path.join(dir, regressions_in_branch['page'])
        with open(regressions_page, 'w') as output_file:
            template = env.get_template('regressions.html.j2')
            html_output = template.render(now=now,
                                          regressions=regressions_in_branch,
                                          analysis=analysis_cache)
            output_file.write(html_output)

    # Infra issues output: main index
    infra_data = _process_infra_issues()
    with open(os.path.join(dir, 'infra_issues.html'), 'w') as output_file:
        template = env.get_template('infra_issues.html.j2')
        html_output = template.render(now=now, infra=infra_data)
        output_file.write(html_output)
    # One page per lab
    for lab in infra_data['labs']:
        with open(os.path.join(dir, lab + '.html'), 'w') as output_file:
            template = env.get_template('lab.html.j2')
            html_output = template.render(now=now, lab=lab, infra_issues=infra_data)
            output_file.write(html_output)
    with open(os.path.join(dir, 'bisections.html'), 'w') as output_file:
        template = env.get_template('bisections.html.j2')
        html_output = template.render(now=now, bisections=bisections)
        output_file.write(html_output)

    with open(os.path.join(dir, 'status.html'), 'w') as output_file:
        template = env.get_template('status.html.j2')
        html_output = template.render(now=now, status=status)
        output_file.write(html_output)

    files = ["main.css", "main.js", "regressions.js"]
    for f in files:
        shutil.copy(os.path.join(sys.path[0], 'templates', f), os.path.join(sys.path[0], dir))
    shutil.copytree(os.path.join(sys.path[0], 'templates', 'images'), os.path.join(sys.path[0], dir, 'images'))

    logging.info(f"HTML reports generated at `{dir}/`")
