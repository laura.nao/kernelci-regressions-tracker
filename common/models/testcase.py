# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

class TestCase:
    """Platform-agnostic definition of an individual test case (or test
    run)
    """
    def __init__(self,
                 test_name,
                 tree,
                 branch,
                 kernel,
                 commit,
                 arch,
                 device_type,
                 compiler,
                 defconfig,
                 date,
                 lab,
                 status):
        self.test_name   = test_name
        self.tree        = tree
        self.branch      = branch
        self.kernel      = kernel
        self.commit      = commit
        self.arch        = arch
        self.device_type = device_type
        self.compiler    = compiler
        self.defconfig   = defconfig
        self.date        = date
        """Test case date as a datetime object"""
        self.lab         = lab
        self.status      = status

    def __str__(self):
        return (f"name          : {self.test_name}\n"
                f"tree          : {self.tree}\n"
                f"branch        : {self.branch}\n"
                f"kernel        : {self.kernel}\n"
                f"commit        : {self.commit}\n"
                f"arch          : {self.arch}\n"
                f"device_type   : {self.device_type}\n"
                f"compiler      : {self.compiler}\n"
                f"defconfig     : {self.defconfig}\n"
                f"date          : {self.date}\n"
                f"lab           : {self.lab}\n"
                f"status        : {self.status}")

    def summary_str(self):
        return (f"{self.tree}/{self.branch} {self.test_name}: "
                f"{self.status} on {self.kernel}, "
                f"{self.date}")

    def __eq__(self, other):
        return all([self.test_name   == other.test_name,
                    self.tree        == other.tree,
                    self.branch      == other.branch,
                    self.kernel      == other.kernel,
                    self.commit      == other.commit,
                    self.arch        == other.arch,
                    self.device_type == other.device_type,
                    self.compiler    == other.compiler,
                    self.defconfig   == other.defconfig,
                    self.date        == other.date,
                    self.lab         == other.lab,
                    self.status      == other.status])

    def __lt__(self, other):
        return self.date < other.date
