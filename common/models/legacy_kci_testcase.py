# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import urllib

from .testcase import TestCase
from ..legacy_kci_api import query_test_case
from ..utils import process_date_param

class LegacyKCITestCase(TestCase):
    """Models a test case as defined by the legacy KernelCI"""

    db_table = 'legacykci_testcase'
    """Name to use for the database table to store LegacyKCITestCase
    objects"""

    def __init__(self,
                 api_url,
                 test_id,
                 test_name,
                 tree,
                 branch,
                 kernel,
                 commit,
                 arch,
                 device_type,
                 compiler,
                 defconfig,
                 # Date as an ISO 8601 format string or as reported by
                 # the KernelCI API (number of milliseconds since the
                 # epoch
                 date,
                 lab,
                 status,
                 group_id=None,
                 regression_id=None):
        super().__init__(test_name=test_name,
                         tree=tree,
                         branch=branch,
                         kernel=kernel,
                         commit=commit,
                         arch=arch,
                         device_type=device_type,
                         compiler=compiler,
                         defconfig=defconfig,
                         date=process_date_param(date),
                         lab=lab,
                         status=status)
        self.api_url       = api_url
        self.test_id       = test_id
        self.link          = self.test_link()
        self.group_id      = group_id
        self.regression_id = regression_id

    def test_link(self):
        if 'chromeos' in self.api_url:
            return f"https://chromeos.kernelci.org/test/case/id/{self.test_id}/"
        else:
            return f"https://kernelci.org/test/case/id/{self.test_id}/"

    def _log(self, suffix):
        if 'chromeos' in self.api_url:
            base_url = "https://storage.chromeos.kernelci.org/kernelci"
        else:
            base_url = "https://storage.kernelci.org"
        log_name = (self.test_name.split('.')[0] + '-' +
                    self.device_type + f'.{suffix}')
        table = str.maketrans({'/' : '-',
                               ':' : '-'})
        parts = [x.translate(table) for x in [self.tree,
                                              self.branch,
                                              self.kernel,
                                              self.arch,
                                              self.defconfig,
                                              self.compiler,
                                              self.lab,
                                              log_name]]
        return (urllib.parse.urljoin(base_url, '/'.join(parts)))

    def html_log(self):
        return self._log('html')

    def txt_log(self):
        return self._log('txt')

    def summary_str(self):
        # For now show the link to the KernelCI dashboard as the test
        # summary
        return self.link

    def __str__(self):
        string = (f"Backend       : {self.api_url}\n"
                  f"Test case     : {self.test_id}\n"
                  f"test_group    : {self.group_id}\n"
                  f"Plain log     : {self.txt_log()}\n"
                  f"HTML log      : {self.html_log()}\n") + super().__str__() + '\n'
        if self.regression_id:
            string += f"regression_id : {self.regression_id}"
        return string

    def __eq__(self, other):
        return self.test_id == self.test_id

    @classmethod
    def from_query(cls, data):
        if 'regression_id' in data and data['regression_id']:
            regression_id = data['regression_id']['$oid']
        else:
            regression_id = None
        test = cls(api_url=data['api_url'],
                   test_id=data['_id']['$oid'],
                   test_name=data['test_case_path'],
                   tree=data['job'],
                   branch=data['git_branch'],
                   kernel=data['kernel'],
                   commit=data['git_commit'],
                   arch=data['arch'],
                   device_type=data['device_type'],
                   compiler=data['build_environment'],
                   defconfig=data['defconfig_full'],
                   date=data['created_on']['$date'],
                   lab=data['lab_name'],
                   status=data['status'],
                   group_id=data['test_group_id']['$oid'],
                   regression_id=regression_id)
        return test

    @classmethod
    def from_id(cls, api_url, test_id):
        data = query_test_case(api_url, test_id)
        data['api_url'] = api_url
        test = cls.from_query(data)
        return test

    # ORM methods
    @classmethod
    def create_table(cls, conn, regression_table, regression_pkey):
        """Creates a DB table to store LegacyKCITestCase objects using an
        established db connection. The primary key will be the test_id,
        and each LegacyKCITestCase can connect to one regression via a
        foreign key to the regression table and primary key specified as
        parameters.

        If the table exists already, this has no effect.
        """
        cur = conn.cursor()
        cur.execute(f"""CREATE TABLE IF NOT EXISTS {cls.db_table} (
        api_url       TEXT,
        test_id       TEXT,
        test_name     TEXT,
        tree          TEXT,
        branch        TEXT,
        kernel        TEXT,
        commit_hash   TEXT,
        arch          TEXT,
        device_type   TEXT,
        compiler      TEXT,
        defconfig     TEXT,
        date          TEXT,
        lab           TEXT,
        status        TEXT,
        group_id      TEXT,
        regression_id TEXT,

        PRIMARY KEY(api_url, test_id),
        FOREIGN KEY(api_url, regression_id) REFERENCES {regression_table}({regression_pkey}));""")

    @classmethod
    def search(cls, conn, match_params=None, query_cond=None):
        """Searches and retrieves a list of matching LegacyKCITestCase objects
        from the database. The database is accessed through an
        established connection and it must contain a table for
        LegacyKCITestCases (see create_table). The search parameters are
        given as a {'parameter': 'value'} dict.
        """
        cur = conn.cursor()
        query_str = f"SELECT * FROM {cls.db_table}"
        if match_params:
            cond_list = []
            for k, v in match_params.items():
                cond_list.append(f"{k} = '{v}'")
            cond_str = ' AND '.join(cond_list)
            query_str += f" WHERE {cond_str}"
        elif query_cond:
            query_str += f" WHERE {query_cond}"
        cur.execute(query_str)
        tests = []
        for t in cur:
            tests.append(cls(*t))
        return tests

    @classmethod
    def custom_query(cls, conn, fields=None, cond=None, join=None, extra=None):
        cur = conn.cursor()
        if fields:
            fields_str = ', '.join(fields)
        else:
            fields_str = '*'
        query_str = f"SELECT {fields_str} FROM {cls.db_table}"
        if join:
            query_str += f" {join}"
        if cond:
            query_str += f" WHERE {cond}"
        if extra:
            query_str += f" {extra}"
        return cur.execute(query_str).fetchall()

    def save(self, conn, replace=True):
        """Saves a LegacyKCITestCase object into the database using an
        established DB connection.

        The database must contain a table for LegacyKCITestCases (see
        create_table)
        """
        op = 'REPLACE'
        if not replace:
            op = 'IGNORE'
        cur = conn.cursor()
        cur.execute(f"INSERT OR {op} INTO {self.db_table} VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    (self.api_url, self.test_id, self.test_name, self.tree,
                     self.branch,    self.kernel,      self.commit,
                     self.arch,      self.device_type, self.compiler,
                     self.defconfig, self.date,        self.lab,
                     self.status,    self.group_id,    self.regression_id))
        conn.commit()

    def delete(self, conn):
        cur = conn.cursor()
        cur.execute(f"DELETE FROM {self.db_table} WHERE test_id='{self.test_id}'")
        conn.commit()

def find_test_cases(api_url, arch=None, branch=None, date_range=None,
                    defconfig=None, device_type=None, kernel=None,
                    lab_name=None,  status=None,      test_name=None,
                    tree=None,      compiler=None,    limit=None,
                    sort_field=None, sort_ascending=False, retry=True):
    """Queries the API for test cases matching a set of optional parameters
    and returns the results as a list of LegacyKCITestCases.

    All the query parameters are optional. If no parameters are passed,
    it retrieves all the tests run in the past day.

    Keep in mind that certain parameter combinations or lack of parameters
    might cause the query to timeout.
    """
    if not any([arch, branch, defconfig, device_type,
                kernel, lab_name, status, test_name, tree, compiler]):
        if not date_range or date_range < 1:
            date_range = 1
    plan = None
    tests = []
    if test_name:
        plan = test_name.split('.')[0]
    params = {
        'arch'           : arch,
        'date_range'     : date_range,
        'defconfig_full' : defconfig,
        'device_type'    : device_type,
        'git_branch'     : branch,
        'kernel'         : kernel,
        'job'            : tree,
        'lab_name'       : lab_name,
        'status'         : status,
        'plan'           : plan,
        'build_environment': compiler,
        'sort'           : sort_field,
        'limit'          : limit
    }
    if sort_ascending:
        params['sort_order'] = 1
    results = query_test_case(api_url, params=params, retry=retry)
    for t in results:
        t['api_url'] = api_url
        # Post-query filtering: if the test name was a query parameter,
        # return only the tests matching it
        if test_name:
            if t['test_case_path'] == test_name:
                tests.append(LegacyKCITestCase.from_query(t))
        else:
            tests.append(LegacyKCITestCase.from_query(t))
    return tests
