# Copyright (C) 2023 Collabora Limited
# Author: Gustavo Padovan <gustavo.padovan@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import logging
import re
import requests
import xml.dom.minidom

from common.utils import get_http_data, start_https_session

# Based on the Lore code taken from:
# https://github.com/kernelci/kernelci-core/blob/main/kernelci/bisect.py

LORE_URL = "https://lore.kernel.org/all/"
session = None

def _lore_get_entries(text):
    """Process lore server response.

        Parse the xml dom received and return
        a list of entries.
    """
    dom = xml.dom.minidom.parseString(text)
    entries = dict()
    feed = dom.childNodes[0]
    for feed_node in feed.childNodes:
        if feed_node.tagName == 'entry':
            entry = feed_node
            title = None
            url = None
            for entry_node in entry.childNodes:
                if entry_node.tagName == 'title':
                    title = entry_node.firstChild.nodeValue
                elif entry_node.tagName == 'link':
                    url = entry_node.getAttribute('href')
            if title and url:
                entries[title] = url
    return entries

def get_lore_url(subject):
    """Search for lore url for a give patch subject

        Send a query to lore server, parse the results,
        matching for the patch subject.

        Return:
            - the lore url or None if not found.
    """
    global session

    if not session:
        session = start_https_session(LORE_URL)

    # remove '*' from query as that breaks the search
    query = subject.replace('*', '')
    try:
        resp = get_http_data(session, LORE_URL, params={'x': 'A', 'q': query})
    except requests.exceptions.HTTPError as err:
        logging.error(f"Failed to get Lore URL: {err}.")
        return None

    entries = _lore_get_entries(resp.text)
    if not entries:
        return None

    matches = dict()
    RE_SUBJECT = re.compile(
        r'^\[PATCH[\ ]?(?P<prefix>[^\d^\ ]*)?[\ ]?'
        r'(v(?P<pver>\d+))?[\ ]?(?P<pnum>\d+\/\d+)?]'
        r'\ (?P<title>.*)$'
    )
    for title, url in entries.items():
        match = RE_SUBJECT.match(title)
        if match is None:
            continue
        groups = match.groupdict()
        if not groups['title'].startswith(subject):
            continue
        matches[int(groups['pver'] or 0)] = url
    urls = list(matches[x] for x in sorted(matches.keys()))
    if not urls:
        return None
    return urls[-1]
