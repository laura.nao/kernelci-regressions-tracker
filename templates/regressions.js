/*
 * Copyright (C) 2023 Collabora Limited
 * Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
 *
 * This module is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * Display filter functions for the regressions index
 */

/*
 * Updates the visibility of all regression report entries. Hide a
 * regression report entry if all its individual regressions are hidden,
 * show it otherwise.
 */
function update_display_reports() {
    reports = document.querySelectorAll(".regression_report")
    for (report of reports) {
        let visible_children = false;
        regs = report.querySelectorAll(".regression")
        for (r of regs) {
            if (r.style.display != "none") {
                visible_children = true;
                break;
            }
        }
        if (visible_children) {
            report.style.display = "";
        } else {
            report.style.display = "none";
        }
    }
}

/*
 * Shows all regressions
 */
function show_all() {
    sel = document.querySelectorAll(".regression")
    for (r of sel) {
        r.style.display = "";
    }
    update_display_reports();
}

/*
 * Hides all regressions
 */
function hide_all() {
    sel = document.querySelectorAll(".regression")
    for (r of sel) {
        r.style.display = "none";
    }
    update_display_reports();
}

/*
 * Handles the behavior, visibility and relationships between the
 * widgets of the Filter controls
 */
function filter_controls_handler() {
    showAll = document.getElementById('showAllRadio')
    showOnly = document.getElementById('showOnlyRadio')
    showBisected = document.getElementById('showBisected')
    showNew = document.getElementById('showNew')
    showRest = document.getElementById('showRest')
    showAnalyzedFlaky = document.getElementById('showAnalyzedFlaky')
    showAnalyzedFixed = document.getElementById('showAnalyzedFixed')

    function trigger_show_all() {
        showOnly.checked = false;
        showAll.checked = true;
        showBisected.disabled = true;
        showBisected.checked = false;
        showNew.disabled = true;
        showNew.checked = false;
        showAnalyzedFlaky.disabled = true;
        showAnalyzedFlaky.checked = false;
        showAnalyzedFixed.disabled = true;
        showAnalyzedFixed.checked = false;
        showRest.disabled = true;
        showRest.checked = false;
        show_all();
    }

    function update_visibility() {
        hide_all();
        if (showBisected.checked) {
            sel = document.querySelectorAll(".regression.bisected")
            for (r of sel) {
                r.style.display = "";
            }
        }
        if (showNew.checked) {
            sel = document.querySelectorAll(".regression.new")
            for (r of sel) {
                r.style.display = "";
            }
        }
        if (showAnalyzedFlaky.checked) {
            sel = document.querySelectorAll(".regression.analyzed_flaky")
            for (r of sel) {
                r.style.display = "";
            }
        }
        if (showAnalyzedFixed.checked) {
            sel = document.querySelectorAll(".regression.analyzed_fixed")
            for (r of sel) {
                r.style.display = "";
            }
        }
        if (showRest.checked) {
            sel = document.querySelectorAll(".regression:not(.new):not(.bisected):not(.analyzed_flaky):not(.analyzed_fixed)")
            for (r of sel) {
                r.style.display = "";
            }
        }
        update_display_reports();
    }

    function check_checkboxes() {
        /* None checked */
        if (!showBisected.checked      &&
            !showNew.checked           &&
            !showRest.checked          &&
            !showAnalyzedFlaky.checked &&
            !showAnalyzedFixed.checked) {
            trigger_show_all();
            return;
        }
        /* All checked */
        if (showBisected.checked       &&
            showNew.checked            &&
            showRest.checked           &&
            showAnalyzedFlaky.checked  &&
            showAnalyzedFixed.checked) {
            show_all();
        } else {
            update_visibility();
        }
    }

    showAll.addEventListener('change', trigger_show_all);
    showOnly.addEventListener('change', function() {
        showBisected.disabled = false;
        showBisected.checked = true;
        showNew.disabled = false;
        showNew.checked = true;
        showAnalyzedFlaky.disabled = false;
        showAnalyzedFlaky.checked = true;
        showAnalyzedFixed.disabled = false;
        showAnalyzedFixed.checked = true;
        showRest.disabled = false;
        showRest.checked = true;
        show_all();
    });
    showBisected.addEventListener('change', check_checkboxes);
    showNew.addEventListener('change', check_checkboxes);
    showAnalyzedFlaky.addEventListener('change', check_checkboxes);
    showAnalyzedFixed.addEventListener('change', check_checkboxes);
    showRest.addEventListener('change', check_checkboxes);
    trigger_show_all();
}

document.addEventListener("DOMContentLoaded", filter_controls_handler);
